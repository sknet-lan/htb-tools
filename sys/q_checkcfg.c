/* 
 *    q_checkcfg.c 
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */



#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "lists.h"
#include "parse_cfg.h"


/******* net_misc *******/

int
is_in_net (unsigned h, unsigned net, unsigned fh, unsigned lh)
{

  struct in_addr addr;
  unsigned long int hnet, hh;

  addr.s_addr = h;
  hh = inet_lnaof (addr);
  hnet = inet_netof (addr);

  return ((net == hnet) && (hh >= fh) && (hh <= lh));
}

void
nnet_addr (int *n, int *fh, int *lh, char *h, int m)
{
  struct in_addr fa;
  int no_ip;

  inet_aton (h, &fa);
  no_ip = 1 << (32 - m);
  *fh = inet_lnaof (fa);
  *n = inet_netof (fa);
  *fh -= *fh % no_ip;
  *lh = *fh + no_ip - 1;
  fa = inet_makeaddr (*n, *lh);
  *lh = inet_lnaof (fa);
  no_ip = *lh - *h;
}


int
find_ip (char *net, char *ip)
{
  int n, fh, lh, bmask;
  char tnet[64];
  char *tc;

  bmask = atoi (index (net, '/') + 1);
  strncpy (tnet, net, 64);
  tc = index (tnet, '/');
  *tc = '\0';

  nnet_addr (&n, &fh, &lh, tnet, bmask);

  return is_in_net (inet_addr (ip), n, fh, lh);
}

/************************/

extern struct q_class *classes_list;
extern int default_bandwidth;

extern struct q_class *crt_class;
extern struct q_client *crt_client;


int
summary_check_cfg ()
{
  struct q_class *q;
  struct q_client *c;
  int tot_cir, tot_mir, tot_cir2, tot_mir2, class_count;

  q = classes_list;
  tot_cir = tot_mir = tot_cir2 = tot_mir2 = class_count = 0;

  printf ("Default bandwidth: %d\n\n", default_bandwidth);

  while (q)
    {
      int cir, mir, client_count;
      printf ("Class %s, CIR: %d, MIR: %d\n", q->name, q->bandwidth,
	      q->limit);
      tot_cir += q->bandwidth;
      tot_mir += q->limit;
      class_count++;
      cir = mir = client_count = 0;
      c = q->clients_list;
      while (c)
	{
	  cir += c->bandwidth;
	  mir += c->limit;
	  client_count++;
	  c = c->next;
	}
      printf ("\t** %d clients, CIR2: %d, MIR2: %d\n\n", client_count, cir,
	      mir);
      tot_cir2 += cir;
      tot_mir2 += mir;
      q = q->next;
    }
  printf ("%d classes; CIR / MIR = %d / %d; CIR2 / MIR2 = %d / %d\n",
	  class_count, tot_cir, tot_mir, tot_cir2, tot_mir2);

  return 0;
}

void
entry_info (struct q_class *q, struct q_client *c, char *ip)
{
  int i;
  puts (ip);
  printf ("\tClass: %s (%d/%d)\n", q->name, q->bandwidth, q->limit);
  printf ("\t\tClient: %s (%d/%d)\n", c->name, c->bandwidth, c->limit);
  for (i = 0; i < c->src_nr; i++)
    printf ("\t\t\tSrc net %d: %s\n", i + 1, c->src[i]);
  for (i = 0; i < c->dst_nr; i++)
    printf ("\t\t\tDst net %d: %s\n", i + 1, c->dst[i]);
}

int
ip_check_cfg (char *ip)
{
  int i;
  struct q_class *q;
  struct q_client *c;

  q = classes_list;

  while (q)
    {
      c = q->clients_list;
      while (c)
	{
	  for (i = 0; i < c->src_nr; i++)
	    if (find_ip (c->src[i], ip))
	      entry_info (q, c, ip);
	  for (i = 0; i < c->dst_nr; i++)
	    if (find_ip (c->dst[i], ip))
	      entry_info (q, c, ip);
	  c = c->next;
	}
      q = q->next;
    }
  return 0;
}

void
usage (char *app_name)
{
  printf ("Usage:\n%s <cfg_file>\n", app_name);
  printf ("%s [IP1 IP2 .. IPn] <cfg_file>\n\n", app_name);
}

int
main (int argc, char *argv[])
{
  char *cfg_file;
  int find_ip = 0;

  init_classes_list ();

  if (argc == 2)
    cfg_file = argv[1];
  else if (argc > 3 && strcasecmp (argv[1], "FINDIP") == 0)
    {
      cfg_file = argv[argc - 1];
      find_ip = 1;
    }
  else
    {
      usage (argv[0]);
      exit (0);
    }

  read_cfg (cfg_file);

  if (find_ip == 0)
    summary_check_cfg ();
  else
    {
      for (find_ip = 2; find_ip < argc - 1; find_ip++)
	{
	  ip_check_cfg (argv[find_ip]);
	}
    }

  return 0;
}
