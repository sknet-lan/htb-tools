/*
*	q_show.c	Ionut Spirlea	<ionut.spirlea[at]rdsnet.ro>
*			Subredu Manuel	<diablo[at]infoiasi.ro>
*
*    This program is free software; you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*   along with this program; if not, write to the Free Software
*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <limits.h>
#include <libnetlink.h>
#include <ll_map.h>
#define __always_inline
#include <asm/types.h>
#include <asm/bitops.h>
#include <linux/pkt_sched.h>
#include <linux/pkt_cls.h>
#include <linux/inetdevice.h>

#include "parse_cfg.h"
#include "lists.h"

int filter_ifindex;
__u32 filter_qdisc;

typedef unsigned long long ull;

struct _qclass
{
  int class_id[2];
  int parent_id[2];
  int leaf[2];
  struct tc_stats st;
  struct tc_stats rcv;
};

struct q_default default_class;

#define MAX_NAME_LEN	128
#define MAX_MATCH_STRINGS 32

#define VERSION "0.3.0"

char interface_name[MAX_NAME_LEN];
char cfg_file[MAX_NAME_LEN];
char match_str[MAX_MATCH_STRINGS][MAX_NAME_LEN];
int kbytes = 0;			/* show rate in kbytes/sec (0-false, 1-true) */
int one = 0;
int match_strings_nr = 0;	//number of strings to search for a match
int show_default = 1;		//show default class ?
int first = 0;
int only_classes = 0;		//show only classes ?
int threshold = 0;
int show_all = 0;

extern struct q_class *classes_list;
extern struct q_class *crt_class;
extern struct q_client *crt_client;
extern int default_bandwidth;

static int counter = 0;		/* 0 .. 8 */

void sigalrm_h (int);

int
print_help ()
{
  printf ("Usage: q_show <args> [options]\n\n");
  printf ("\tArguments :\n");
  printf ("\t\t--interface,\t-i <interface>\n");
  printf ("\t\t--file,\t\t-f <config file>\n");
  printf ("\n\tOptions :\n");
  printf ("\t\t--match,\t-m strings list delimited by \",\" or \";\"\n");
  printf ("\t\t--kbytes,\t-k show all values in kbytes (default kbit)\n");
  printf ("\t\t--one,\t\t-1 show only once\n");
  printf ("\t\t--classes,\t-c show only classes\n");
  printf ("\t\t--nodefault,\t-D do not show default class (default on)\n");
  printf ("\t\t--all,\t\t-a show all clients of a class (default off)\n");
  printf
    ("\t\t--threshold,\t-t <integer> do not show clients that have less traffic than this number\n");
  printf ("\t\t--help,\t\t-h this help\n");
  printf ("\t\t--version,\t-V print version and exit\n");

  return 1;
}

int
print_version ()
{
  printf ("version: %s\n", VERSION);
  return 1;
}

char
string_match (char *str)
{
  int i;

  for (i = 0; i < match_strings_nr; i++)
    if (strstr (str, match_str[i]) != NULL)
      return 1;

  return 0;
}

void
cleanup (int sig)
{
  free_classes_list ();
  exit (0);
}

void
update_classes_list (int id0, int id1, unsigned long long sent, unsigned pkts) 
{

  struct q_class *q = NULL;
  struct q_client *c = NULL;
  int q_client_count;
  int q_class_count = 0x20;

  if (id0 == 1 && id1 == 0x10)
    {

      default_class.qs.bps[counter] =
	(float) (sent - default_class.qs.sent) / 0.333333;
      if ((counter % 3) == 0)
	default_class.qs.pps[counter / 3] = pkts - default_class.qs.pkts;
      if (first == 0)
	{
	  default_class.qs.bps[counter] = 0;
	  default_class.qs.pps[counter / 3] = 0;
	}
      default_class.qs.sent = sent;
      default_class.qs.pkts = pkts;

      return;
    }

  q = classes_list;
  while (q)
    {
      if (id0 == 1 && id1 == q_class_count)
	{
	  q->qs.bps[counter] = (float) (sent - q->qs.sent) / 0.333333;
	  if ((counter % 3) == 0)
	    q->qs.pps[counter / 3] = pkts - q->qs.pkts;
	  if (first == 0)
	    {
	      q->qs.bps[counter] = 0;
	      q->qs.pps[counter / 3] = 0;
	    }
	  q->qs.sent = sent;
	  q->qs.pkts = pkts;
	  return;
	}
      q_client_count = q_class_count + 1;
      c = q->clients_list;
      while (c)
	{
	  if (id0 == 1 && id1 == q_client_count)
	    {
	      c->qs.bps[counter] = (float) (sent - c->qs.sent) / 0.333333;
	      if ((counter % 3) == 0)
          c->qs.pps[counter / 3] = pkts - c->qs.pkts;
	      if (first == 0)
		{
		  c->qs.bps[counter] = 0;
		  c->qs.pps[counter / 3] = 0;
		}
          c->qs.sent = sent;
	      c->qs.pkts = pkts;
	      return;
	    }
	  q_client_count += 1;
	  c = c->next;
	}
      q_class_count = q_client_count;
      q = q->next;
    }
}

int
get_class (struct sockaddr_nl *who, struct nlmsghdr *n, void *arg)
{
  struct tcmsg *t = NLMSG_DATA (n);
  int len = n->nlmsg_len;
  struct rtattr *tb[TCA_MAX + 1];

  struct _qclass qclass;

  memset (&qclass, 0, sizeof (struct _qclass));

  if (n->nlmsg_type != RTM_NEWTCLASS && n->nlmsg_type != RTM_DELTCLASS)
    {
      fprintf (stderr, "Not a class\n");
      return 0;
    }
  len -= NLMSG_LENGTH (sizeof (*t));
  if (len < 0)
    {
      fprintf (stderr, "Wrong len %d\n", len);
      return -1;
    }
  if (filter_qdisc && TC_H_MAJ (t->tcm_handle ^ filter_qdisc))
    return 0;

  memset (tb, 0, sizeof (tb));
  parse_rtattr (tb, TCA_MAX, TCA_RTA (t), len);

  if (tb[TCA_KIND] == NULL)
    {
      fprintf (stderr, "NULL kind\n");
      return -1;
    }

  if (n->nlmsg_type == RTM_DELTCLASS)
    printf ("deleted ");

  if (t->tcm_handle)
    {
      __u32 h;
      if (filter_qdisc)
	{
	  h = TC_H_MIN (t->tcm_handle);
	  if (h == TC_H_ROOT)
	    qclass.class_id[0] = qclass.class_id[1] = 0;
	  else if (h == TC_H_UNSPEC)
	    qclass.class_id[0] = qclass.class_id[1] = -1;
	  else if (TC_H_MAJ (h) == 0)
	    qclass.class_id[0] = 0, qclass.class_id[1] = TC_H_MIN (h);
	  else if (TC_H_MIN (h) == 0)
	    qclass.class_id[0] = TC_H_MAJ (h) >> 16, qclass.class_id[1] = 0;
	  else
	    qclass.class_id[0] = TC_H_MAJ (h) >> 16, qclass.class_id[1] = TC_H_MIN (h);
	}
      else
	{
	  h = t->tcm_handle;
	  if (h == TC_H_ROOT)
	    qclass.class_id[0] = qclass.class_id[1] = 0;
	  else if (h == TC_H_UNSPEC)
	    qclass.class_id[0] = qclass.class_id[1] = -1;
	  else if (TC_H_MAJ (h) == 0)
	    qclass.class_id[0] = 0, qclass.class_id[1] = TC_H_MIN (h);
	  else if (TC_H_MIN (h) == 0)
	    qclass.class_id[0] = TC_H_MAJ (h) >> 16, qclass.class_id[1] = 0;
	  else
	    qclass.class_id[0] = TC_H_MAJ (h) >> 16, qclass.class_id[1] = TC_H_MIN (h);
	}
    }

  if (t->tcm_parent == TC_H_ROOT)
    qclass.parent_id[0] = qclass.parent_id[1] = 0;
  else
    {
      __u32 h;
      if (filter_qdisc)
	{
	  h = TC_H_MIN (t->tcm_parent);
	  if (h == TC_H_ROOT)
	    qclass.parent_id[0] = qclass.parent_id[1] = 0;
	  else if (h == TC_H_UNSPEC)
	    qclass.parent_id[0] = qclass.parent_id[1] = -1;
	  else if (TC_H_MAJ (h) == 0)
	    qclass.parent_id[0] = 0, qclass.parent_id[1] = TC_H_MIN (h);
	  else if (TC_H_MIN (h) == 0)
	    qclass.parent_id[0] = TC_H_MAJ (h) >> 16, qclass.parent_id[1] = 0;
	  else
	    qclass.parent_id[0] = TC_H_MAJ (h) >> 16, qclass.parent_id[1] = TC_H_MIN (h);
	}
      else
	{
	  h = t->tcm_parent;
	  if (h == TC_H_ROOT)
	    qclass.parent_id[0] = qclass.parent_id[1] = 0;
	  else if (h == TC_H_UNSPEC)
	    qclass.parent_id[0] = qclass.parent_id[1] = -1;
	  else if (TC_H_MAJ (h) == 0)
	    qclass.parent_id[0] = 0, qclass.parent_id[1] = TC_H_MIN (h);
	  else if (TC_H_MIN (h) == 0)
	    qclass.parent_id[0] = TC_H_MAJ (h) >> 16, qclass.parent_id[1] = 0;
	  else
	    qclass.parent_id[0] = TC_H_MAJ (h) >> 16, qclass.parent_id[1] = TC_H_MIN (h);
	}
    }

  if (t->tcm_info)
    qclass.leaf[0] = 0, qclass.leaf[1] = t->tcm_info >> 16;

  if (tb[TCA_STATS])
    {
      if (RTA_PAYLOAD (tb[TCA_STATS]) < sizeof (struct tc_stats))
	fprintf (stderr, "statistics truncated");
      else
	memcpy (&qclass.st, RTA_DATA (tb[TCA_STATS]),sizeof (struct tc_stats));


  	memcpy (&qclass.rcv, RTA_DATA (tb[TCA_STATS]),sizeof (struct tc_stats));
   }

  update_classes_list (qclass.class_id[0], qclass.class_id[1],qclass.st.bytes, qclass.st.packets); 
  return 0;
}


int
qclass_list (char *dev)
{
  struct tcmsg t;
  struct rtnl_handle rth;

  memset (&t, 0, sizeof (t));
  t.tcm_family = AF_UNSPEC;


  if (rtnl_open (&rth, 0) < 0)
    {
      fprintf (stderr, "Cannot open rtnetlink\n");
      exit (1);
    }

  ll_init_map (&rth);

  if ((t.tcm_ifindex = ll_name_to_index (dev)) == 0)
    {
      fprintf (stderr, "Cannot find device \"%s\"\n", dev);
      exit (1);
    }
  filter_ifindex = t.tcm_ifindex;

  if (rtnl_dump_request (&rth, RTM_GETTCLASS, &t, sizeof (t)) < 0)
    {
      perror ("Cannot send dump request");
      exit (1);
    }

  if (rtnl_dump_filter (&rth, get_class, (FILE *) NULL, NULL, NULL) < 0)
    {
      fprintf (stderr, "Dump terminated\n");
      exit (1);
    }

  close (rth.fd);

  counter = (counter + 1) % 9;

  return 0;
}

void
print_cl (struct q_client *c)
{
  float speed;
  float upload; //
  unsigned pps;
  int i;

  if ((strcmp (match_str[0], "NONE") && !string_match (c->name))
      || only_classes)
    return;

  for (i = 0, speed = 0; i <= 8; speed += c->qs.bps[i], i++);
for (i = 0, upload = 0; i <= 8; upload += c->qs.upload[i], i++); //a
  for (i = 0, pps = 0; i <= 2; pps += c->qs.pps[i], i++);

  speed /= (kbytes == 0) ? (9.0 * 1024.0 / 8.0) : (9.0 * 1024.0);
upload /= (kbytes == 0) ? (9.0 * 1024.0 / 8.0) : (9.0 * 1024.0); //a
  pps /= 3;

  if (speed > threshold || show_all)
    c->ttl = 5;
  else if (c->ttl != 0)
    c->ttl--;

  if (c->ttl != 0)
//    printf ("  %-28s %10.2f %10.2f %7u %9d %9d\n", c->name, speed, upload, pps,c->bandwidth, c->limit);
    printf ("  %-28s %15.2f %7u %9d %9d\n", c->name, speed, pps,c->bandwidth, c->limit);

}

void
print_cs (struct q_class *q)
{
  float speed;
  float upload;//a
  unsigned pps;
  int i;

  if (strcmp (match_str[0], "NONE") && (!string_match (q->name)))
    return;
  for (i = 0, speed = 0; i <= 8; speed += q->qs.bps[i], i++);
for (i = 0, upload = 0; i <= 8; upload += q->qs.upload[i], i++); //a
  
  for (i = 0, pps = 0; i <= 2; pps += q->qs.pps[i], i++);

  speed /= (kbytes == 0) ? (9.0 * 1024.0 / 8.0) : (9.0 * 1024.0);
upload /= (kbytes == 0) ? (9.0 * 1024.0 / 8.0) : (9.0 * 1024.0); //a
  pps /= 3;

//  printf ("%-30s %10.2f %10.2f %7u %9d %9d\n", q->name, speed, upload, pps, q->bandwidth,q->limit);
  printf ("%-30s %15.2f %7u %9d %9d\n", q->name, speed, pps, q->bandwidth,q->limit);

}

void
print_default_class ()
{
  float speed;
float upload; //a
  unsigned pps;
  int i;

  for (i = 0, speed = 0; i <= 8; speed += default_class.qs.bps[i], i++);
for (i = 0, upload = 0; i <= 8; upload += default_class.qs.bps[i], i++); //a
  for (i = 0, pps = 0; i <= 2; pps += default_class.qs.pps[i], i++);

  speed /= (kbytes == 0) ? (9.0 * 1024.0 / 8.0) : (9.0 * 1024.0);
upload /= (kbytes == 0) ? (9.0 * 1024.0 / 8.0) : (9.0 * 1024.0); //a
  
  pps /= 3;

  printf ("%-30s %15.2f %7u %9d %9d\n", " _DEFAULT_", speed, pps, default_bandwidth, default_bandwidth);

}

void
puts_qs ()
{
  struct q_class *q = NULL;
  struct q_client *c = NULL;

  printf ("\033[H\033[J");

  q = classes_list;
  while (q)
    {
      print_cs (q);
      c = q->clients_list;
      while (c)
	{
	  print_cl (c);
	  c = c->next;
	}
      q = q->next;
    }

  if (show_default)
    print_default_class ();
}

/* added by Diablo */
void
do_it_in_kbytes ()
{
  struct q_class *q = NULL;
  struct q_client *c = NULL;

  q = classes_list;
  while (q)
    {

      q->bandwidth /= 8;
      q->limit /= 8;

      c = q->clients_list;
      while (c)
	{
	  c->bandwidth /= 8;
	  c->limit /= 8;
	  c = c->next;
	}
      q = q->next;
    }
}

void
split_match_strings (char *str)
{
  char buffer[4096];
  char *atoms;

  strncpy (buffer, str, sizeof (buffer));
  atoms = strtok (buffer, ",;");
  strncpy (match_str[match_strings_nr++], atoms, MAX_NAME_LEN);

  while ((atoms = strtok (NULL, ",;")) != NULL
	 && match_strings_nr < MAX_MATCH_STRINGS)
    strncpy (match_str[match_strings_nr++], atoms, MAX_NAME_LEN);
}

char
config_file_ok (char *file)
{
  struct stat inf;

  if (stat (file, &inf) == -1)
    return 0;
  else
    return 1;
}

int
q_show ()
{
  static int c = 0;
  if (one == 0)
    {
      puts_qs ();
    }
  else if (++c == 4)
    {
      puts_qs ();
      cleanup (2);
    }
  return 0;
}

static int
parse_args (int argc, char *argv[])
{
  static struct option long_options[12] = {
    {"interface", 1, 0, 'i'},
    {"file", 1, 0, 'f'},
    {"all", 0, 0, 'a'},
    {"threshold", 1, 0, 't'},
    {"match", 1, 0, 'm'},
    {"help", 0, 0, 'h'},
    {"version", 0, 0, 'V'},
    {"kbytes", 0, 0, 'k'},
    {"one", 0, 0, '1'},
    {"nodefault", 0, 0, 'D'},
    {"classes", 0, 0, 'c'},
    {0, 0, 0, 0}
  };

  int c, h_interface;

  strcpy (cfg_file, "/etc/qos.cfg");
  strcpy (interface_name, "NONE");
  strcpy (match_str[0], "NONE");

  h_interface = 0;

  while (1)
    {
      int option_index = 0;

      c =
	getopt_long (argc, argv, "i:f:m:t:hDcVk1a", long_options,
         &option_index);

      if (c == -1)
	break;

      switch (c)
	{
	case 0:

	case 'h':
	  print_help ();
	  exit (0);
	  break;

	case 'V':
	  print_version ();
	  exit (0);
	  break;

	case 'f':
	  strcpy (cfg_file, optarg);
	  break;

	case 'i':
	  h_interface = 1;
	  strcpy (interface_name, optarg);
	  break;

	case 'm':
	  split_match_strings (optarg);
	  break;

	case 'k':
	  kbytes = 1;		/* added by diablo */
	  break;

	case 'D':
	  show_default = 0;
	  break;

	case '1':
	  one = 1;
	  break;

	case 'c':
	  only_classes = 1;
	  break;
	case 't':
	  threshold = atoi (optarg);
	  break;
	case 'a':
	  show_all = 1;
	  break;

	}
    }
  if (!h_interface)
    {
      puts ("\nError: interface missing.\n");
      print_help ();		/* added by diablo */
      exit (0);
    }

  if (!config_file_ok (cfg_file))
    {
      puts ("\nError: config file missing.\n");
      print_help ();		/* added by diablo */
      exit (0);

    }

  return 1;
}

int
main (int argc, char *argv[])
{
  struct itimerval v;

  signal (SIGINT, cleanup);
  signal (SIGHUP, cleanup);
  signal (SIGTERM, cleanup);

  signal (SIGALRM, sigalrm_h);

  parse_args (argc, argv);

  init_classes_list ();

  read_cfg (cfg_file);

  /* added by Diablo */
  if (kbytes == 1)
    do_it_in_kbytes ();


  v.it_interval.tv_sec = 0;
  v.it_interval.tv_usec = 333333;	/* 3 @ 3 */
  v.it_value.tv_sec = 0;
  v.it_value.tv_usec = 333333;
  setitimer (ITIMER_REAL, &v, NULL);

  for (;; getchar ());		/* main_loop ;) */

  return 0;
}

void
sigalrm_h (int sig)
{
  static int show = 1;
  qclass_list (interface_name);
  first = 1;
  if (show == 0)
    q_show ();
  show = (show + 1) % 3;
}
