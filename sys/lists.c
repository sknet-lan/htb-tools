/*
*	Project :
* 	Module  : classes list implementation
* 	-- Vasile Laurentiu Stanimir (stanimir[at]cr.nivis.com)
*	 12.06.2001 - v 0.1
*
*   + me (ionut.spirlea[at]rdsnet.ro)
*
*
*    This program is free software; you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation; either version 2 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software
*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lists.h"

struct q_class *classes_list;
int default_bandwidth;

struct q_class *crt_class;
struct q_client *crt_client;

int
init_classes_list ()
{
  classes_list = NULL;
  crt_class = classes_list;
  return 1;
}

int
open_next_class ()
{
  struct q_class *c;

  if ((c = (struct q_class *) malloc (sizeof (struct q_class))) != NULL)
    {
      if (crt_class)
	{
	  crt_class->next = c;
	  crt_class = crt_class->next;
	}
      else
	{
	  classes_list = c;
	  crt_class = classes_list;
	}
      insert_in_class (BANDWIDTH_PARAM, 0);
      insert_in_class (LIMIT_PARAM, 0);
      insert_in_class (BURST_PARAM, 0);
      insert_in_class (PRIORITY_PARAM, 0);
      insert_in_class (QUE_PARAM, 0);
      memset (&(crt_class->qs), 0, sizeof (struct _q_show));
      crt_class->clients_list = NULL;
      crt_class->next = NULL;
      return 1;
    }
  return 0;
}

int
insert_in_class (int param, int value)
{
  switch (param)
    {
    case BANDWIDTH_PARAM:
      crt_class->bandwidth = value;
      break;
    case LIMIT_PARAM:
      crt_class->limit = value;
      break;
    case BURST_PARAM:
      crt_class->burst = value;
      break;
    case PRIORITY_PARAM:
      crt_class->priority = value;
      break;
    }
  return 1;
}

int
insert_in_class_str (int param, char *value)
{
  switch (param)
    {
    case QUE_PARAM:
      strncpy (crt_class->que, value, MAX_QUE_NAME);
//                  printf ("#Que:%s\n",crt_class->que);
      break;
    }
  return 1;
}

int
set_class_name (char *name)
{
  strncpy (crt_class->name, name, MAX_NAME);
  strncpy (crt_class->que, "pfifo limit 5", MAX_NAME);
  return 1;
}

/*
int set_class_que(char *que)
{
	strncpy(crt_class->que, que, MAX_NAME);
	return 1;
}
*/

int
init_clients_list ()
{

  crt_class->clients_list = NULL;
  crt_client = crt_class->clients_list;
  return 1;
}

int
open_next_client ()
{
  struct q_client *c;
  if ((c = (struct q_client *) malloc (sizeof (struct q_client))) != NULL)
    {
      if (crt_client)
	{
	  crt_client->next = c;
	  crt_client = crt_client->next;
	}
      else
	{
	  crt_class->clients_list = c;
	  crt_client = crt_class->clients_list;
	}
      insert_in_client (BANDWIDTH_PARAM, 0);
      insert_in_client (LIMIT_PARAM, 0);
      insert_in_client (BURST_PARAM, 0);
      insert_in_client (PRIORITY_PARAM, 0);
      insert_in_client (MARK_PARAM, 0);
      insert_in_client (UPLOAD_PARAM, 0);
      crt_client->dst_nr = 0;
      crt_client->src_nr = 0;
      memset (&(crt_client->qs), 0, sizeof (struct _q_show));
      crt_client->next = NULL;
      return 1;
    }
  return 0;
}

int
insert_in_client (int param, int value)
{
  switch (param)
    {
    case BANDWIDTH_PARAM:
      crt_client->bandwidth = value;
      break;
    case LIMIT_PARAM:
      crt_client->limit = value;
      break;
    case BURST_PARAM:
      crt_client->burst = value;
      break;
    case PRIORITY_PARAM:
      crt_client->priority = value;
      break;
    case MARK_PARAM:
      crt_client->mark = value;
      break;
    case UPLOAD_PARAM:
      crt_client->upload = value;
      break; 

   }
  return 1;
}

int
insert_dst_in_client (char *dst)
{
  strncpy (crt_client->dst[crt_client->dst_nr], dst, MAX_NET_LEN);
  crt_client->dst_nr++;
  return 1;
}

int
insert_src_in_client (char *src)
{
  strncpy (crt_client->src[crt_client->src_nr], src, MAX_NET_LEN);
  crt_client->src_nr++;
  return 1;
}

int
set_client_name (char *name)
{
  strncpy (crt_client->name, name, MAX_NAME);
  return 1;
}

int
set_default_bandwidth (int db)
{
  default_bandwidth = db;
  return 1;
}

int
free_classes_list ()
{
  struct q_class *q, *qn;
  struct q_client *c, *cn;

  q = classes_list;
  while (q)
    {
      c = q->clients_list;
      while (c)
	{
	  cn = c->next;
	  free (c);
	  c = cn;
	}
      qn = q->next;
      free (q);
      q = qn;
    }
  return 1;
}

int
debug_write_list ()
{
  int i;
  struct q_class *q;
  struct q_client *c;

  q = classes_list;

  printf ("\n\n DEBUG WRITE LIST \n\n");
  printf ("Default bandwidth: %d\n\n", default_bandwidth);
  while (q)
    {
      printf ("Class %s\n", q->name);
      printf ("   Bandwidth: %d\n", q->bandwidth);
      printf ("   Limit    : %d\n", q->limit);
      printf ("   Burst    : %d\n", q->burst);
      printf ("   Priority : %d\n", q->priority);
      c = q->clients_list;
      while (c)
	{
	  printf ("   Client %s\n", c->name);
	  printf ("      Bandwidth: %d\n", c->bandwidth);
	  printf ("      Limit    : %d\n", c->limit);
	  printf ("      Burst    : %d\n", c->burst);
	  printf ("      Priority : %d\n", c->priority);
	  printf ("      Mark     : %d\n", c->mark);
	  printf ("      Upload   : %d\n", c->upload);
//                      printf("      Src net  : %s\n", c->src);
	  for (i = 0; i < c->dst_nr; i++)
	    printf ("      Dst net %d: %s\n", i + 1, c->dst[i]);

	  c = c->next;
	}
      printf ("\n");
      q = q->next;
    }
  return 0;
}
