/*
*  	q_parser.c
*	copyleft tech@rdscv.ro
*
*	many thanks: Vasile Stanimir for flex implementation
*	and many thanks to Walt Disney.
*
*    This program is free software; you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation; either version 2 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software
*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "parse_cfg.h"
#include "lists.h"

#define TC	"TC=/sbin/tc"
#define U32up   "U32u=\"filter add dev $DEV protocol ip parent ffff: prio 1 u32\"\n\n"
#define U32	"U32=\"filter add dev $DEV protocol ip parent 1:0 prio 1 u32\""
#define VERSION "0.3.0"

extern struct q_class *classes_list;
extern int default_bandwidth;
extern int errno;

extern struct q_class *crt_class;
extern struct q_client *crt_client;

unsigned int
get_max_burst ()
{
  struct q_class *q;
  unsigned int max_burst = 0;

  q = classes_list;

  int bu;

  while (q)
    {
      if (q->burst == 0){
         bu = (float) (q->limit) * 0.30;
      } else {
         bu = q->burst;
      }    
      if (max_burst < bu)
        max_burst = bu;
    
      q = q->next;
    }
  return max_burst;
}

struct address *
parse_addr (char *addr)
{
  char buffer[512];
  char *atoms;
  struct port_list *port;
  struct port_list *begin_port;
  struct address *return_addr;

  if ((return_addr =
       (struct address *) malloc (sizeof (struct address))) == NULL)
    {
      perror ("Could not allocate memory\n");
      exit (errno);
    }

  bzero (return_addr, sizeof (struct address));

  port = begin_port = NULL;
  strncpy (buffer, addr, sizeof (buffer));

  atoms = strtok (buffer, " ,\t\r\n;");	//this should be address
  strncpy (return_addr->address, atoms, sizeof (return_addr->address));
  return_addr->ports = NULL;

  while ((atoms = strtok (NULL, " ,\t\n\r;")) != NULL)
    {
      struct port_list *temp;

      if ((temp =
	   (struct port_list *) malloc (sizeof (struct port_list))) == NULL)
	{
	  perror ("Could not allocate memory\n");
	  exit (errno);
	}

      if (port == NULL)
	{
	  port = begin_port = temp;
	  port->next_port = NULL;
	  port->port_nr = atoi (atoms);
	}
      else
	{
	  port->next_port = temp;
	  port = temp;
	  port->next_port = NULL;
	  port->port_nr = atoi (atoms);
	}
    }

  return_addr->ports = begin_port;
  return return_addr;
}

void
destroy_address_struct (struct address *addr)
{
  struct port_list *p, *f;

  p = addr->ports;
  while (p)
    {
      f = p->next_port;
      free (p);
      p = f;
    }
  free (addr);
}

unsigned int
gen_script (char *dev, unsigned int root_class_rate,
	    unsigned int root_class_limit)
{
  unsigned int q_client_count;
  unsigned int i, j;
  struct q_class *q;
  struct q_client *c;
  struct port_list *ps, *pd;
  unsigned int max_burts;
  unsigned int q_class_count = 0x10;
  char class_que[MAX_QUE_NAME];

  q = classes_list;

  puts ("#!/bin/sh\n\n");
  printf ("DEV=%s\n", dev);
  puts (TC);
  puts (U32);
  puts (U32up);

  puts ("echo Delete previous root qdisc");

  puts ("$TC qdisc del dev $DEV root >/dev/null 2>&1");
  puts ("$TC qdisc del dev $DEV ingress  >/dev/null 2>&1\n");  

  puts ("echo Add root qdisc");
  puts ("$TC qdisc add dev $DEV handle ffff: ingress;  >/dev/null 2>&1");  
  puts ("$TC qdisc add dev $DEV root handle 1: htb default 10");

  max_burts = get_max_burst ();

  puts ("echo Add root class");
  printf
     ("$TC class add dev $DEV parent 1: classid 1:1 htb rate %dkbps ceil %dkbps burst %dk quantum 1536\n",
     root_class_rate / 8, root_class_limit / 8, max_burts);

  puts ("echo Add default class");

  printf("$TC class add dev $DEV parent 1:1 classid 1:0x%x htb rate %dkbps ceil %dkbps burst %dk quantum 1536\n", q_class_count, default_bandwidth / 8, default_bandwidth / 8, max_burts);
  printf("$TC qdisc add dev $DEV parent 1:0x%x handle 0x%x: pfifo limit 5\n\n",
     q_class_count, q_class_count);


  q_class_count += 0x10;

      int bq;
      int bc;
      int bu;
      
  while (q)
    {
      if (q->burst == 0){
          bq = (float) (q->limit) * 0.30;
        }else{
          bq = q->burst;
        }
      strncpy (class_que, q->que, MAX_QUE_NAME);	//setting the que 2 use beneath the classes
      printf(" echo \"\"\n echo Add class %s, que %s\n ", q->name, class_que );
      printf(" $TC class add dev $DEV parent 1:1 classid 1:0x%x htb rate %dkbps ceil %dkbps burst %dk prio %d quantum 1536\n", q_class_count, q->bandwidth / 8, q->limit / 8, bq, q->priority);

      q_client_count = q_class_count + 1;
      c = q->clients_list;
    
      while (c)
        {
          if (c->burst == 0){
            bc = (float) (c->limit) * 0.30;
          }else{
            bc = c->burst;
          }
  	      printf("      echo -e -n \"\r\" Add client %s\"                 \"\n",c->name);
          
	      if (c->bandwidth / 8 >= 15)
            {
	          printf("   $TC class add dev $DEV parent 1:0x%x classid 1:0x%x htb rate %dkbps ceil %dkbps burst %dk prio %d quantum 1536\n", q_class_count, q_client_count, c->bandwidth / 8, c->limit 
              / 8, bc, c->priority);
	        }else{
	          printf("   $TC class add dev $DEV parent 1:0x%x classid 1:0x%x htb rate %dkbps ceil %dkbps burst %dk prio %d quantum 1536\n", q_class_count, q_client_count, c->bandwidth / 8, c->limit
               / 8, bc, c->priority);
            }
            
          if ((c->dst_nr == 0 ) && (c->src_nr == 0) && c->mark > 0){  // avem doar mark

            printf ("    $TC ${U32} match mark %d 0xffff flowid 1:0x%x\n", c->mark, q_client_count);
//          printf ("   $TC filter add dev $DEV parent 1:0 protocol ip prio 1 handle %d fw classid 1:0x%x\n", c->mark, q_client_count); 
          printf("    $TC qdisc add dev $DEV parent 1:0x%x handle 0x%x: %s\n", q_client_count, q_client_count, class_que);

          q_client_count += 1;
          c = c->next;
          continue;


          }else if((c->src_nr != 0) && (c->dst_nr == 0)){ // awe have only src in config

	        for (j = 0; j < c->src_nr; j++)
		      {
		        struct address *s_addr;
		        s_addr = parse_addr (c->src[j]);
		        ps = s_addr->ports;

		        if (ps == NULL)
		          {
		          printf ("    $TC ${U32} match ip src %s", s_addr->address);
			  
		          if (c->mark > 0)
    			    printf (" match mark %d 0xffff ", c->mark);
                  printf (" flowid 1:0x%x\n ", q_client_count);
		        } 

		        while (ps)
		          {
		            if (strncmp (s_addr->address, "0.0.0.0", 7) == 0)
			          printf ("    $TC ${U32}");
		            else
			          printf ("    $TC ${U32} match ip src %s ", s_addr->address);
				  
				  printf (" match ip sport %d 0xffff", ps->port_nr);
                        
		            if (c->mark > 0)
    			      printf (" match mark %d 0xffff ", c->mark);
                    printf (" flowid 1:0x%x\n ", q_client_count);
		            ps = ps->next_port;
		          }
		          destroy_address_struct (s_addr);
		      }
	          
              printf("    $TC qdisc add dev $DEV parent 1:0x%x handle 0x%x: %s\n", q_client_count, q_client_count, class_que);
              q_client_count += 1;
	          c = c->next;
	          continue;

        }else if((c->dst_nr != 0) && (c->src_nr == 0)){ // we ahve only dst in config

	          for (j = 0; j < c->dst_nr; j++)
		        {
		        struct address *d_addr;
		        d_addr = parse_addr (c->dst[j]);
		        pd = d_addr->ports;

		        if (pd == NULL){
		          printf ("    $TC ${U32} match ip dst %s",
			      d_addr->address);
		          if (c->mark > 0)
                    printf (" match mark %d 0xffff ", c->mark);
		          printf (" flowid 1:0x%x\n", q_client_count);
		        }
		        while (pd)
		          {
		          if (strncmp (d_addr->address, "0.0.0.0", 7) == 0)
			        printf ("    $TC ${U32}");
		          else
			        printf ("    $TC ${U32} match ip dst %s", d_addr->address);
		          printf (" match ip dport %d 0xffff ", pd->port_nr);
		          if (c->mark > 0)
			        printf (" match mark %d 0xffff ", c->mark);
		        printf (" flowid 1:0x%x\n", q_client_count);
		        pd = pd->next_port;
		        }
                if (c->upload > 0){
                  bu = (float) (c->upload) * 0.30;
                  printf ("    $TC ${U32u} match ip src %s police rate %dkbit burst %dk drop flowid :1 \n",d_addr->address,c->upload,bu);
                }

		        destroy_address_struct (d_addr);
		      }
	          
              printf("    $TC qdisc add dev $DEV parent 1:0x%x handle 0x%x: %s\n", q_client_count, q_client_count, class_que);
	          q_client_count += 1;
	          c = c->next;
	          continue;

        }else{ // we have both src and dst in config
	
            for (i = 0; i < c->dst_nr; i++)
	            for (j = 0; j < c->src_nr; j++)
	              {
		          struct address *d_addr;
		          struct address *s_addr;

		          d_addr = parse_addr (c->dst[i]);
		          s_addr = parse_addr (c->src[j]);

		          if (d_addr->ports != NULL && s_addr->ports != NULL)
		            {
			    
		            pd = d_addr->ports;

		            while (pd)
		              {
			          ps = s_addr->ports;
			          while (ps)
			            {
			            if (strncmp (d_addr->address, "0.0.0.0", 7) == 0)
			              printf ("    $TC ${U32}");
			            else
			              printf ("    $TC ${U32} match ip dst %s", d_addr->address);
			          
                        printf (" match ip sport %d 0xffff ", pd->port_nr);
			            if (strncmp (s_addr->address, "0.0.0.0", 7) != 0)
			              printf (" match ip src %s", s_addr->address);
				    
			            printf (" match ip sport %d 0xffff ", ps->port_nr);
				    
			            if (c->mark > 0)
			              printf (" match mark %d 0xffff ", c->mark);
			            printf (" flowid 1:0x%x\n", q_client_count);
			            ps = ps->next_port;
			          }
			          pd = pd->next_port;
		            }
		          destroy_address_struct (d_addr);
		          destroy_address_struct (s_addr);
		          continue;
		        }

		        if (d_addr->ports != NULL)
		          {
			
		          pd = d_addr->ports;
		          while (pd)
		            {
			        if (strncmp (d_addr->address, "0.0.0.0", 7) == 0)
			          printf ("    $TC ${U32}");
			        else
			          printf ("    $TC ${U32} match ip dst %s",  d_addr->address);
				  

				  
			        printf (" match ip sport %d 0xffff ", pd->port_nr);
			        
				if (strncmp (s_addr->address, "0.0.0.0", 7) != 0)
			          printf (" match ip src %s", s_addr->address);
			          if (c->mark > 0)
			            printf (" match mark %d 0xffff ", c->mark);
			          printf (" flowid 1:0x%x\n", q_client_count);
			          pd = pd->next_port;
		            }
		          destroy_address_struct (d_addr);
		          destroy_address_struct (s_addr);
		          continue;
		        }
			
		        if (s_addr->ports != NULL)

		            {
		            ps = s_addr->ports;
		            while (ps)
		              {
			          if (strncmp (d_addr->address, "0.0.0.0", 7) == 0)
			            printf ("    $TC ${U32}");
			          else
			            printf ("    $TC ${U32} match ip dst %s", d_addr->address);
			          if (strncmp (s_addr->address, "0.0.0.0", 7) != 0)
			            printf (" match ip src %s", s_addr->address);
			          printf (" match ip sport %d 0xffff ", ps->port_nr);
			          if (c->mark > 0)
			            printf (" match mark %d 0xffff ", c->mark);
			          printf (" flowid 1:0x%x\n", q_client_count);
			          ps = ps->next_port;
		              }
		            destroy_address_struct (d_addr);
		            destroy_address_struct (s_addr);
		           continue;
		        }

		        printf ("    $TC ${U32} match ip dst %s", d_addr->address);
		        printf (" match ip src %s", s_addr->address);
		        if (c->mark > 0)
		          printf (" match mark %d 0xffff ", c->mark);
		        printf (" flowid 1:0x%x\n", q_client_count);
		        destroy_address_struct (d_addr);
		        destroy_address_struct (s_addr);
 	}
	  
      printf("    $TC qdisc add dev $DEV parent 1:0x%x handle 0x%x: %s\n", q_client_count, q_client_count, class_que);
	  q_client_count += 1;
	  c = c->next;
	};
}
      q_class_count = q_client_count;

      q = q->next;
      printf ("\n");
      printf(" echo \"\"\n");

    };
  return 0;
}

int
main (int argc, char *argv[])
{

  if (argc != 5)
    {
      printf("Usage: %s <device> <root_class_rate> <root_class_limit> <cfg_file>\n",argv[0]);
      return 1;
    }
  init_classes_list ();
  read_cfg (argv[4]);

  gen_script (argv[1], atoi (argv[2]), atoi (argv[3]));
  free_classes_list ();

  return 0;

}

