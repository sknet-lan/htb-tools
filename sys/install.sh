#!/bin/sh
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Author:     arny  <arny[at]arny[dot]ro>
#
if [ `id -u` -ne 0 ]; then
        echo "This script must be run as root!"
        exit -1
fi
echo " "
echo "Installing HTB-tools ..."
        /bin/cp -v q_parser q_show q_checkcfg sys/scripts/htb sys/scripts/htbgen /sbin
	/bin/chmod +x /sbin/htb /sbin/htbgen
echo " "
echo "Installing default config files in /etc/htb... "
if [ ! -x /etc/htb ]; then 
	echo "Creating directory /etc/htb ..."
        mkdir /etc/htb
	   else
	echo "Directory /etc/htb exist. Skipping ..."
	echo " "
fi
        echo " Installing default config files ..." 

if [ -r /etc/htb/eth0-qos.cfg ]; then
	 echo "eth0-qos.cfg file found in /etc/htb. Config file installation aborted."
	      else
         echo "eth0-qos.cfg NOT FOUND, installing ..."
         /bin/cp cfg/eth0-qos.cfg /etc/htb/
         echo "eth0-qos.cfg installed."
fi
	 echo " "

if [ -r /etc/htb/eth1-qos.cfg ]; then
         echo "eth1-qos.cfg file found in /etc/htb. Config file installation aborted."
	      else
         echo "eth1-qos.cfg NOT FOUND, installing ..."
         /bin/cp cfg/eth1-qos.cfg /etc/htb/
         echo "eth1-qos.cfg installed in /etc/htb."
         echo " "
	 echo " Config files installation DONE."

fi

    sleep 1
  	 echo " "
         echo "Installing rc.htb init script. Please enter the corect init scripts path: "
	 echo "( slackware type init should use /etc/rc.d; RedHat type init should use /etc/init.d )"
	 read cale
		if [ -x $cale ]; then
			echo "Installing rc.htb to $cale ..."
			/bin/cp sys/scripts/rc.htb $cale
			/bin/chmod +x $cale/rc.htb
		sleep 1
		echo "Done."
		else
		echo "Try again, directory $cale does not exist on your system. Exiting."
		fi
