%{

#define yywrap() 1

/* 
 * Project:
 * Module : flex config_file_parser
 * -- Vasile Laurentiu Stanimir (stanimir[at]cr.nivis.com, http://cr.nivis.com/~stanimir)
 * 12.05.2001 - v 0.1
 *
 */


#include <stdio.h>
#include "lists.h"

int h_default		= 0;

int h_bandwidth 	= 0;
int h_limit 	  	= 0;
int h_burst 		= 0;
int h_priority 	 	= 0;
int h_que		= 0;

int h_c_bandwidth	= 0;
int h_c_limit		= 0;
int h_c_burst		= 0;
int h_c_priority	= 0;
int h_c_dst		= 0;
int h_c_src       	= 0;
int h_c_mark            = 0;
int h_c_upload	        = 0;

%}

%option yylineno


%x EXPECT_CLASS_ID
%x EXPECT_END_CLASS_ID
%x EXPECT_CLASS_QUE
%x EXPECT_END_CLASS_QUE
%x EXPECT_CLASS_BODY
%x EXPECT_END_DEFAULT_CLASS_ID
%x EXPECT_DEFAULT_BANDWIDTH
%x EXPECT_DEFAULT_BANDWIDTH_VALUE
%x EXPECT_END_DEFAULT_CLASS
%x EXPECT_BANDWIDTH
%x EXPECT_LIMIT
%x EXPECT_BURST
%x EXPECT_PRIORITY
%x EXPECT_CLIENT_ID
%x EXPECT_END_CLIENT_ID
%x EXPECT_CLIENT_BODY
%x EXPECT_CLIENT_BANDWIDTH
%x EXPECT_CLIENT_LIMIT
%x EXPECT_CLIENT_BURST
%x EXPECT_CLIENT_PRIORITY
%x EXPECT_CLIENT_MARK
%x EXPECT_CLIENT_UPLOAD
%x EXPECT_CLIENT_DST
%x EXPECT_CLIENT_DST_BODY
%x EXPECT_END_CLIENT_DST_ITEM
%x EXPECT_END_CLIENT_ITEM
%x EXPECT_END_ITEM
%x EXPECT_CLIENT_SRC_BODY
%x EXPECT_END_CLIENT_SRC_ITEM

SPACES                  [[:blank:]]*
NUMBER                  [[:digit:]]+
PORT                    [0-9 ,]*


COMMENT_SHELL           "#"[^"\n"]*"\n"
COMMENT_CPP		"//".*"\n"
COMMENT_C		"/*".*"*/"


IP_ADDRESS              {NUMBER}"."{NUMBER}"."{NUMBER}"."{NUMBER}
ID_NET                  {IP_ADDRESS}{SPACES}"/"{SPACES}{NUMBER}{SPACES}{PORT}
NAME			[a-z0-9_-]*	
ID_CLASS		{NAME}
ID_QUE			{NAME}
ID_CLIENT		{NAME}
ID_BANDWIDTH		{NUMBER}
ID_LIMIT		{NUMBER}
ID_BURST		{NUMBER}
ID_PRIORITY		{NUMBER}
ID_MARK                 {NUMBER}
ID_UPLOAD               {NUMBER}
%%

<INITIAL>"class"		{
	h_limit = 0;
	h_bandwidth = 0;
	h_burst = 0;
	h_priority = 0;
	h_que = 0;
	//if(!open_next_class()) {
	//	printf("Error opening class!");
	//	yyterminate();
	//}
	//init_clients_list();
	
	BEGIN EXPECT_CLASS_ID;
}

<EXPECT_CLASS_ID>"default"	{
	if(!h_default) {
		h_default = 1;
		// printf( "Default class\n");
		//set_class_name("DEFAULT");
		BEGIN EXPECT_END_DEFAULT_CLASS_ID;
	} else {
		printf("Error at line %d (Duplicate default entry).\n", yylineno);
		yyterminate();
	}
}

<EXPECT_CLASS_ID>{ID_CLASS}	{
//	printf( "#CLASS: %s\n",yytext );
	open_next_class();
	init_clients_list();
	set_class_name(yytext);
	BEGIN EXPECT_END_CLASS_ID;
}

<EXPECT_END_DEFAULT_CLASS_ID>"{"	{
	BEGIN EXPECT_DEFAULT_BANDWIDTH;
}

<EXPECT_DEFAULT_BANDWIDTH>"bandwidth"{SPACES} {
	BEGIN EXPECT_DEFAULT_BANDWIDTH_VALUE;
}

<EXPECT_DEFAULT_BANDWIDTH_VALUE>{ID_BANDWIDTH} {
	// printf("   Bandwidth: %s\n", yytext);
	set_default_bandwidth(atoi(yytext));
	//insert_in_class(BANDWIDTH_PARAM, atoi(yytext));
	BEGIN EXPECT_END_DEFAULT_CLASS;
}

<EXPECT_END_DEFAULT_CLASS>{SPACES}";"{SPACES}"}"{SPACES}";"{SPACES}  {
	BEGIN INITIAL;
}

<EXPECT_END_CLASS_ID>"{" {
	BEGIN EXPECT_CLASS_BODY;
}

<EXPECT_CLASS_BODY>"}"{SPACES}";"	{
	if( !h_bandwidth || !h_limit ||
	    !h_burst || !h_priority ) {
		printf("Error at line %d (Missing one or more statements).\n", yylineno);
		yyterminate();
	} else {
		// printf( "End CLASS\n\n");
		BEGIN INITIAL;
	}
}

<EXPECT_CLASS_BODY>"bandwidth"		{
	BEGIN EXPECT_BANDWIDTH;
}

<EXPECT_BANDWIDTH>{ID_BANDWIDTH} {
	if(!h_bandwidth) {
		h_bandwidth = 1;
		// printf("   Bandwidth: %s\n", yytext);
		insert_in_class(BANDWIDTH_PARAM, atoi(yytext));
		BEGIN EXPECT_END_ITEM;
	} else {
		printf("Error at line %d (Duplicate bandwidth).\n ", yylineno);
		yyterminate();
	}
}

<EXPECT_CLASS_BODY>"limit"		{
	BEGIN EXPECT_LIMIT;
}

<EXPECT_LIMIT>{ID_LIMIT}	{
	if(!h_limit) {
		h_limit = 1;
		// printf("   Limit: %s\n", yytext);
		insert_in_class(LIMIT_PARAM, atoi(yytext));
		BEGIN EXPECT_END_ITEM;
	} else {
		printf("Error at line %d (Duplicate limit).\n", yylineno);
		yyterminate();
	}
}

<EXPECT_CLASS_BODY>"burst"	{
	BEGIN EXPECT_BURST;
}

<EXPECT_BURST>{ID_BURST}	{
	if(!h_burst) {
		h_burst = 1;
		// printf("   Burst: %s\n", yytext);
		insert_in_class(BURST_PARAM, atoi(yytext));
		BEGIN EXPECT_END_ITEM;
	} else {
		printf("Error at line %d (Duplicate burst).\n", yylineno);
		yyterminate();
	}
}

<EXPECT_CLASS_BODY>"que"	{
	BEGIN EXPECT_CLASS_QUE;
}

<EXPECT_CLASS_QUE>{ID_QUE}	{
	if(!h_que) {
		h_que = 1;
		//printf("   que: %s\n", yytext);
		insert_in_class_str(QUE_PARAM, yytext);
		BEGIN EXPECT_END_ITEM;
	} else {
		printf("Error at line %d (Duplicate que).\n", yylineno);
		yyterminate();
	}
}

<EXPECT_CLASS_BODY>"priority"	{
	BEGIN EXPECT_PRIORITY;
}

<EXPECT_PRIORITY>{ID_PRIORITY}	{
	if(!h_priority) {
		h_priority = 1;
		// printf("   Priority: %s\n", yytext);
		insert_in_class(PRIORITY_PARAM, atoi(yytext));
		BEGIN EXPECT_END_ITEM;
	} else {
		printf("Error at line %d (Duplicate priority).\n", yylineno);
		yyterminate();
	}
}

<EXPECT_CLASS_BODY>"client"	{
	h_c_bandwidth = 0;
	h_c_limit = 0;
	h_c_burst = 0;
	h_c_priority = 0;
        h_c_mark = 0;
        h_c_upload = 0;
	h_c_dst = 0;
	h_c_src = 0;
	open_next_client();
	BEGIN EXPECT_CLIENT_ID;
}


<EXPECT_CLIENT_ID>{ID_CLIENT}	{
	// printf("   Client: %s\n", yytext);
	set_client_name(yytext);
	BEGIN EXPECT_END_CLIENT_ID;
}

<EXPECT_END_CLIENT_ID>"{"	{
	BEGIN EXPECT_CLIENT_BODY;		
}

<EXPECT_CLIENT_BODY>"}"{SPACES}";"	{
	if( !h_c_bandwidth || !h_c_limit ||
	    !h_c_burst || !h_c_priority ||
		(!h_c_dst && !h_c_src && !h_c_mark && !h_c_upload )) {
		printf("Error at line %d (Missing one or more statements).\n", yylineno);
		yyterminate();
	} else {
		// printf("   End CLIENT\n");
		BEGIN EXPECT_CLASS_BODY;
	}
}

<EXPECT_CLIENT_BODY>"bandwidth"		{
	BEGIN EXPECT_CLIENT_BANDWIDTH;
}

<EXPECT_CLIENT_BANDWIDTH>{ID_BANDWIDTH} {
	if(!h_c_bandwidth) {
		h_c_bandwidth = 1;
		// printf("      Bandwidth: %s\n", yytext);
		insert_in_client(BANDWIDTH_PARAM, atoi(yytext));
		BEGIN EXPECT_END_CLIENT_ITEM;
	} else {
		printf("Error at line %d (Duplicate bandwidth).\n ", yylineno);
		yyterminate();
	}
}

<EXPECT_CLIENT_BODY>"limit"		{
	BEGIN EXPECT_CLIENT_LIMIT;
}

<EXPECT_CLIENT_LIMIT>{ID_LIMIT}	{
	if(!h_c_limit) {
		h_c_limit = 1;
		// printf("      Limit: %s\n", yytext);
		insert_in_client(LIMIT_PARAM, atoi(yytext));
		BEGIN EXPECT_END_CLIENT_ITEM;
	} else {
		printf("Error at line %d (Duplicate limit).\n", yylineno);
		yyterminate();
	}
}

<EXPECT_CLIENT_BODY>"burst"	{
	BEGIN EXPECT_CLIENT_BURST;
}

<EXPECT_CLIENT_BURST>{ID_BURST}	{
	if(!h_c_burst) {
		h_c_burst = 1;
		// printf("      Burst: %s\n", yytext);
		insert_in_client(BURST_PARAM, atoi(yytext));
		BEGIN EXPECT_END_CLIENT_ITEM;
	} else {
		printf("Error at line %d (Duplicate burst).\n", yylineno);
		yyterminate();
	}
}

<EXPECT_CLIENT_BODY>"priority"	{
	BEGIN EXPECT_CLIENT_PRIORITY;
}

<EXPECT_CLIENT_PRIORITY>{ID_PRIORITY}	{
	if(!h_c_priority) {
		h_c_priority = 1;
		// printf("      Priority: %s\n", yytext);
		insert_in_client(PRIORITY_PARAM, atoi(yytext));
		BEGIN EXPECT_END_CLIENT_ITEM;
	} else {
		printf("Error at line %d (Duplicate priority).\n", yylineno);
		yyterminate();
	}
}


<EXPECT_CLIENT_BODY>"mark"	{
	BEGIN EXPECT_CLIENT_MARK;
}

<EXPECT_CLIENT_MARK>{ID_MARK}	{
	if(!h_c_mark) {
		h_c_mark = 1;
		// printf("      Mark: %s\n", yytext);
		insert_in_client(MARK_PARAM, atoi(yytext));
		BEGIN EXPECT_END_CLIENT_ITEM;
	} else {
		printf("Error at line %d (Duplicate mark).\n", yylineno);
		yyterminate();
	}
}

<EXPECT_CLIENT_BODY>"upload"	{
	BEGIN EXPECT_CLIENT_UPLOAD;
}

<EXPECT_CLIENT_UPLOAD>{ID_UPLOAD}	{
	if(!h_c_upload) {
		h_c_upload = 1;
		// printf("      Upload: %s\n", yytext);
		insert_in_client(UPLOAD_PARAM, atoi(yytext));
		BEGIN EXPECT_END_CLIENT_ITEM;
	} else {
		printf("Error at line %d (Duplicate upload).\n", yylineno);
		yyterminate();
	}
}


<EXPECT_CLIENT_BODY>"dst"{SPACES}"{"	{
	h_c_dst = 1;
	// printf("      Dst:\n");
	BEGIN EXPECT_CLIENT_DST_BODY;
}

<EXPECT_CLIENT_BODY>"src"{SPACES}"{"   {
   h_c_src = 1;
   BEGIN EXPECT_CLIENT_SRC_BODY;
}
         

<EXPECT_CLIENT_DST_BODY>"}"		{
	if(h_c_dst <2 && h_c_src<2) {
		printf("Error at line %d (No client destination).\n", yylineno);
		yyterminate();
	} 
	BEGIN EXPECT_END_CLIENT_ITEM;
}

<EXPECT_CLIENT_SRC_BODY>"}"		{
   if(h_c_dst <2 && h_c_src<2) {
      printf("Error at line %d (No client destination).\n", yylineno);
      yyterminate();
   }

	BEGIN EXPECT_END_CLIENT_ITEM;
}

<EXPECT_CLIENT_DST_BODY>{ID_NET} {
	h_c_dst++;
//	printf("         %s\n", yytext);
	insert_dst_in_client(yytext);
	BEGIN EXPECT_END_CLIENT_DST_ITEM;
}

<EXPECT_CLIENT_SRC_BODY>{ID_NET}	{
   h_c_src++;
	insert_src_in_client(yytext);
	BEGIN EXPECT_END_CLIENT_SRC_ITEM;
}


<EXPECT_END_CLIENT_DST_ITEM>";"	{
	BEGIN EXPECT_CLIENT_DST_BODY;
}

<EXPECT_END_CLIENT_SRC_ITEM>";"	{
	BEGIN EXPECT_CLIENT_SRC_BODY;
}


<EXPECT_END_CLIENT_ITEM>";"	{
	BEGIN EXPECT_CLIENT_BODY;
}

<EXPECT_END_ITEM>";" 	{
	BEGIN EXPECT_CLASS_BODY;
}

<*>{COMMENT_C}
<*>{COMMENT_CPP}
<*>{COMMENT_SHELL}    

<*>[ \t\n]+    
<*>.                    {
	printf( "Error al line %d (Unknown statement).\n", yylineno );
	yyterminate();
}

%%

int read_cfg(char *cfg_file) {
	yyin = fopen(cfg_file,"rt");

	/* init_classes_list(); */

	yylex();
	fclose(yyin);
	return 0;
}

