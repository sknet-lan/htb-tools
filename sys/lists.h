/*
 * Project :
 * Module  : Classes list
 * -- Vasile Laurentiu Stanimir (stanimir[at]cr.nivis.com)
 * 12.06.2001 - v 0.1
 *
 *  + me (ionut.spirlea[at]rdsnet.ro)
 */

#ifndef LISTS_H__
#define LISTS_H__

#define MAX_DST		1024
#define MAX_SRC		1024
#define MAX_NET_LEN    	64
#define MAX_NAME	512
#define MAX_QUE_NAME	512

#define BANDWIDTH_PARAM	1000
#define LIMIT_PARAM	101
#define BURST_PARAM	102
#define PRIORITY_PARAM	103
#define QUE_PARAM	104
#define MARK_PARAM      105
#define UPLOAD_PARAM    106
#include "lists.h"

struct _q_show
{
  unsigned long long sent;
  unsigned long long receive;
  unsigned pkts;
  float bps[9];
  float pps[3];
  float upload[9];
};

struct q_client
{
  char name[MAX_NAME];
  int dst_nr;
  int src_nr;
  char dst[MAX_DST][MAX_NET_LEN];
  char src[MAX_SRC][MAX_NET_LEN];
  int bandwidth;
  int limit;
  int burst;
  int priority;
  int ttl;
  int mark;
  int upload;
  struct _q_show qs;
  struct q_client *next;
};

struct q_class
{
  char name[MAX_NAME];
  char que[MAX_QUE_NAME];
  int bandwidth;
  int limit;
  int burst;
  int priority;
  struct q_client *clients_list;
  struct _q_show qs;
  struct q_class *next;
};

struct q_default
{
  int bandwidth;
  int limit;
  struct _q_show qs;
};

struct port_list
{
  int port_nr;
  struct port_list *next_port;
};

struct address
{
  char address[24];
  struct port_list *ports;
};

int init_classes_list ();
int open_next_class ();
int insert_in_class (int param, int value);
int insert_in_class_str (int param, char *value);

int init_clients_list ();
int open_next_client ();
int insert_in_client (int param, int value);
int insert_src_in_client (char *src);
int insert_dst_in_client (char *dst);

int free_classes_list ();

int set_default_bandwidth (int db);

int set_class_name (char *);
int set_class_que (char *);
int set_client_name (char *);

#endif /* LISTS_H__ */
