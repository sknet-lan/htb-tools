#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

CC = gcc
FLEX = flex
CFLAGS = -O2 -Wall
LFLAGS =
VERSION=0.3.0a
LIBS=-L./lib -lnetlink -ldl
CFLAGS=-O2 -Wall -g -I./include
SYS_DIR=sys/
OUT_DIR=/sbin
WEB_DIR=/var/www/webhtb
CFG=/etc/htb
MAN_DIR=/usr/local/man
HTB_MAN_DIR=docs/man
OBJS = sys/lists.o lex.yy.o
NLOBJ=lib/ll_map.o lib/libnetlink.o
CP=/bin/cp -v
MK=/bin/mkdir -p
MOD=/bin/chmod +x
RM=/bin/rm -f
BACK=../../

all: 	lex_parser $(OBJS) libnetlink.a
	$(CC) $(CFLAGS) $(LFLAGS) $(OBJS) $(SYS_DIR)q_parser.c -o q_parser
	$(CC) $(SYS_DIR)q_show.c $(CFLAGS) $(LIBS) $(OBJS) -o q_show
	$(CC) $(SYS_DIR)q_checkcfg.c $(CFLAGS) $(LIBS) $(OBJS) -o q_checkcfg
 
lex.yy.o: $(SYS_DIR)lex.yy.c
	$(CC) $(CFLAGS) -c $(SYS_DIR)lex.yy.c 

lex_parser: 	$(SYS_DIR)parse_cfg.l
	$(FLEX) -i $(SYS_DIR)parse_cfg.l

libnetlink.a: $(NLOBJ)
	ar rcs lib/$@ $(NLOBJ)

install:
	$(CP) $(HTB_MAN_DIR)/q_show.8 $(MAN_DIR)/man8/
	cd $(SYS_DIR)scripts && $(CP) htb htbgen $(BACK)
	$(MOD) htbgen htb && $(CP) q_parser q_show q_checkcfg htb htbgen $(OUT_DIR)

full:
	$(CP) $(HTB_MAN_DIR)/q_show.8 $(MAN_DIR)/man8/
	$(SYS_DIR)install.sh
	web/install.sh
arch: clean
	cd .. && tar cvfz HTB-tools-$(VERSION).tar.gz HTB-tools-$(VERSION)

clean:
	$(RM) *.o $(SYS_DIR)*.o lex.yy.c *~ q_parser q_show htb q_checkcfg htbgen lib/*.o lib/*.a 

uninstall:
	$(RM) $(OUT_DIR)/q_parser $(OUT_DIR)/q_checkcfg $(OUT_DIR)/q_show $(OUT_DIR)/htb $(OUT_DIR)/htbgen
	rm -rfi $(CFG)

web:
	$(MK) $(WEB_DIR) && $(CP) "$(SYS_DIR)web/q_show.php" $(WEB_DIR)/index.php
