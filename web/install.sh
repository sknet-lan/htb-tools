#!/bin/bash
# HTB-tools, q_show web install
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Author:     arny  <arny[at]arny.ro>
#

if [ `id -u` -ne 0 ]; then
        echo "This script must be run as root."
        exit -1
fi

echo 
echo "	Enter the directory where you want to install q_show web."
echo "	ex: /var/www/htdocs"
echo "directory: "; 
read cale
echo

if [ -x $cale ]; then
	    echo "Installing files ..." 
	    mkdir -p $cale/webhtb
 	    /bin/cp "web/q_show-web/index.php" $cale/webhtb/index.php
	    /bin/cp -a "web/whtb-tools_cfg_gen" $cale/whtbcfg 	    

            echo "Done."  
            else
            echo "Directory not exist. Exiting"
	    fi

