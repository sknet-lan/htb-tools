<?php
function read_conf($filename){
  $filename = "./conf/".$filename."-qos.cfg";
  if (!is_file($filename)){
    return;
  }
  $chunksize = 1*(1024*1024);
  $buffer = ''; 
  $handle = fopen($filename, 'rb'); 
  while (!feof($handle)) { 
    $buffer = fread($handle, $chunksize); 
  } 
  fclose($handle); 
  $a = split("\n",$buffer);
  $waitForClassName = false;
  $waitForClassDetails = false;
  $waitForClassBandwidth = false;
  $waitForClassLimit = false;
  $waitForClassBurst = false;
  $waitForClassPriority = false;
  $waitForClientName = false;
  $waitForClientDetails = false;
  $waitForClientBandwidth = false;
  $waitForClientLimit = false;
  $waitForClientBurst = false;
  $waitForClientPriority = false;
  $curentClass = -1;
  $class = array();
  for ($i=0;$i<count($a);$i++){
    $b = split(" ",$a[$i]);
    for ($j=0;$j<count($b);$j++){
      if ($b[$j] != " " && $b[$j] != ";" && $b[$j] != "};" && $b[$j] != "{" && $b[$j] != "\n" ){
        $b[$j] = str_replace(";","",$b[$j]);
        
//###### CLASSES AND CLASSES SPECIFICATIONS ######
        if ($waitForClassDetails == true){
          if ($waitForClassBandwidth==true){
            $waitForClassBandwidth = false; 
            $class[$curentClass][bandwidth]=$b[$j]; 
          }
          if ($b[$j] == "bandwidth"){ 
            $waitForClassBandwidth = true; 
          }
          if ($waitForClassLimit==true){ 
            $waitForClassLimit = false; 
            $class[$curentClass][limit]=$b[$j]; 
          }
          if ($b[$j] == "limit"){ 
            $waitForClassLimit = true; 
          }
          if ($waitForClassBurst==true){
            $waitForClassBurst = false; 
            $class[$curentClass][burst]=$b[$j]; 
          }
          if ($b[$j] == "burst"){ 
            $waitForClassBurst = true; 
          }
          if ($waitForClassPriority==true){ 
            $waitForClassPriority=false; 
            $class[$curentClass][prio]=$b[$j]; 
          }
          if ($b[$j] == "priority"){ 
            $waitForClassPriority = true; 
          }
      }
      if ($waitForClassName == true){
        $curentClass = $curentClass +1 ; 
        $class[$curentClass]= array ('name' => $b[$j]);
        $waitForClassName = false; 
        $waitForClassDetails = true; 
        $curentClient = -1; 
      }
      if ($b[$j] == "class"){ 
        $waitForClassName = true; 
        $waitForClientDetails = false; 
      }
//####### CLIENTS AND CLIENTS SPECIFICATION #######
      if ($waitForClientDetails == true){
        if ($waitForClientBandwidth==true){ 
          $waitForClientBandwidth = false; 
          $class[$curentClass][clients][$curentClient][bandwidth] = $b[$j]; 
        }
        if ($b[$j] == "bandwidth"){ 
          $waitForClientBandwidth = true; 
        }
        if ($waitForClientLimit==true){ 
          $waitForClientLimit = false; 
          $class[$curentClass][clients][$curentClient][limit] = $b[$j];
        }
        if ($b[$j] == "limit"){ 
          $waitForClientLimit = true ; 
        }
        if ($waitForClientBurst==true){ 
          $waitForClientBurst = false; 
          $class[$curentClass][clients][$curentClient][burst] = $b[$j];
        }
        if ($b[$j] == "burst"){ 
          $waitForClientBurst = true; 
        }
        if ($waitForClientPriority==true){ 
          $waitForClientPriority = false; 
          $class[$curentClass][clients][$curentClient][prio] = $b[$j];
        }
        if ($b[$j] == "priority"){
          $waitForClientPriority = true;
        }
        if ($waitForClientMatch==true){
          if($matchType=="src" || $matchType=="dst"){
            for ($d=$i;$d<count($a);$d++){
              $b = split("  ",$a[$d]);
              for ($r=0;$r<count($b);$r++){
                if ($b[$r] == "};"){
                  break;
                }
                if ($b[$r]){
                  $b[$r] = str_replace(";","",$b[$r]);
                  array_push($class[$curentClass][clients][$curentClient][match][$matchType],$b[$r]);
                }
              } 
              if ($b[$r] == "};"){
                break;
              }
            } 
            $matchType = "";
        }elseif ($matchType=="mark"){
          $waitForClientMatch = false;
          $class[$curentClass][clients][$curentClient][match][mark] = array();
          $class[$curentClass][clients][$curentClient][match][mark] = $b[$j];
          $matchType = "";
        }
      }
      if ($b[$j] == "mark" || $b[$j] == "src" || $b[$j] == "dst"){
      if ($b[$j] == "src"){  $class[$curentClass][clients][$curentClient][match][src] = array();};
      if ($b[$j] == "dst"){   $class[$curentClass][clients][$curentClient][match][dst]= array();};
      $matchType = $b[$j];
      $waitForClientMatch = true;
    }
  }
  if ($waitForClientName == true){
    $curentClient = $curentClient +1 ;
    $class[$curentClass][clients][$curentClient] = array('name' => $b[$j]);
    $waitForClientName = false;
    $waitForClientDetails = true;
  }
  if ($b[$j] == "client"){
    $waitForClientName = true;
    $waitForClassDetails = false;
  }
//###############################################################    
      }
    }
  }
  return $class;
}
?>