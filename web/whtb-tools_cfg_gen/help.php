<font face=arial size=2>
WHTB_Tools (v-0.1 alpha) is an aplication created to help you to generate on the web browser config files for HTB_Tools (by arny).<br>
This software is under de GNU license. Feel free to use/modify/recommend to other :). <br>
Any sugestions/remarks are welcome at dan.adrian () gmail com or arny () arny ro<br>
<br>
How to use?<br>
<br>
First of all you must generate config file by clicking "Add new config" from menu.<br>
After input the name of interface where you want make shapping just click "save".<br>
Now refresh the page and select from "Aviabile config files" (from menu) interface sufixed by -qos.cfg.<br>
Here you can add new shapping class by clicking "Add class" from the top of page.<br>
Complet the form then click "save".  Don't forget: all fields are required! <br>
You can repet this step if you want add more classes. <br> 
After you generate a first class you can add client/s. An client is a "rule" into shapping qdisc, indentified by filter by match/s. <br>
The matchs can be: src, dst and mark. <br>
You can use more then one match for every client (rule) but minimum one.<br> 
For example to shapping connections form an LAN machine with ip 10.0.0.10 to any destination you will type into scr field: 10.0.0.10/32.So all incoming trafic from any location and outgoing from shapped interface to this ip addres will be shapped.<br>
To shape C class scr will be: 10.0.0.0/24. 
If you want to shape incoming trafic from an specific adress and port match scr must be: address port. and for many ports: adress port1,port2,port3,etc<br>
For any adress you will type 0.0.0.0 and,port1,port2,etc.<br>
Mark section of match allow to shape marked pachets (pachet can be marked with iptables).<br>
<br>
ALL config files are saved to "conf" directory for YOUR WWW directory. Also in config details you can click on "Show config file" from the top of page to see generated config file. You can copy and paste it in another file. 
Don't forget to make conf directory readable/writeable by web server. (chown apache.apache conf)
<br>
Another think. I strongly recommend you to make the WHTB_Tools directory password protected or remove the directory after you generate the conf's.<br>
<br>
For other informations about how it works HTB_Tools please visit www.arny.ro<br>
<br>
Enjoy,<br>
Dan Adrian (zapa)
<br><br><hr><center>
<b>WEB-HTB_Tools</b>
<br>
<i>By Daniel Adrian Gard
