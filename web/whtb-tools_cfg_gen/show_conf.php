<?php
  if (!is_file("./conf/".$file."-qos.cfg")){
    return;
  }
  $chunksize = 1*(1024*1024);
  $buffer = ''; 
  $handle = fopen("./conf/".$file."-qos.cfg", 'rb'); 
  while (!feof($handle)) {
    $buffer.= fread($handle, $chunksize); 
  } 
  fclose($handle); 
  echo "<textarea cols=80 rows=20># Generated for FREE by WHTB_Tools. \n# By Daniel Adrian G.\n\n".$buffer."</textarea>";
?>