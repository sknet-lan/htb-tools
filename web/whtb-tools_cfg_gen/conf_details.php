<?php
  if($drop){
    shell_exec("rm -rf ./conf/".$drop."-qos.cfg");
    echo "Config file droped... <a href=\"add_conf.php\" target=\"main\" style=\"text-decoration: none\">add new?</a>";
    die;
  }
  if (!$conf){
    echo "No config file selected... ";
    die;
  }
  include "read.php";
  $class = read_conf($conf);
//////////////////////////////////////////////////////////////
  if ($edit_client){
    for ($k=0;$k<count($class);$k++){
      if ($class[$k][name]=="default"){
        $defaultClass=$class[$k][bandwidth];  
      }else{
        $output.= "class ".$class[$k][name]." {\n";
        $output.=  "    bandwidth ".$class[$k][bandwidth].";\n";
        $output.=   "    limit ".$class[$k][limit].";\n";
        $output.=   "    burst ".$class[$k][burst].";\n";
        $output.=   "    priority ".$class[$k][prio].";\n";
        for ($v=0;$v<count($class[$k][clients]);$v++){
          $output.=   "    client ".$class[$k][clients][$v][name]." {\n";
          if ($client_name==$class[$k][clients][$v][name]){
            $output.=   "        bandwidth ".$client_bandwidth.";\n";
            $output.=   "        limit ".$client_limit.";\n";
            $output.=   "        burst ".$client_burst.";\n";
            $output.=   "        priority ".$client_priority.";\n";
            if ($match_mark){
              $output.=   "        mark ".$match_mark.";\n";
            }
    
//////////    
            if ($match_src){
            $match_src = split(";",$match_src);
              $output.=   "        src {\n";
              for ($f=0;$f<count($match_src);$f++){
                $output.=   "            ".$match_src[$f].";\n";
              }
              $output.=   "        };\n";
            }
///////////
            if ($match_dst){
            $match_dst = split(";",$match_dst);
              $output.=   "        dst {\n";
              for ($f=0;$f<count($match_dst);$f++){
                $output.=   "            ".$match_dst[$f].";\n";
              }
              $output.=   "        };\n";
            }
///////////
            $output.=   "    };\n";
          }else{
            $output.=   "        bandwidth ".$class[$k][clients][$v][bandwidth].";\n";
            $output.=   "        limit ".$class[$k][clients][$v][limit].";\n";
            $output.=   "        burst ".$class[$k][clients][$v][burst].";\n";
            $output.=   "        priority ".$class[$k][clients][$v][prio].";\n";
            if ($class[$k][clients][$v][match][mark]){
              $output.=   "        mark ".$class[$k][clients][$v][match][mark].";\n";
            }
            if ($class[$k][clients][$v][match][src]){
              $output.=   "        src {\n";
              for ($f=0;$f<count($class[$k][clients][$v][match][src]);$f++){
                $output.=   "            ".$class[$k][clients][$v][match][src][$f].";\n";
              }
              $output.=   "        };\n";
            }
            if ($class[$k][clients][$v][match][dst]){
              $output.=   "        dst {\n";
              for ($f=0;$f<count($class[$k][clients][$v][match][dst]);$f++){
                $output.=   "            ".$class[$k][clients][$v][match][dst][$f].";\n";
              }
            $output.=   "        };\n";
            }
          $output.=   "    };\n";
        }
      
      }
      $output.=   "};";
      $output.=   "\n\n";  
    }
  }
  $output.="\nclass default { bandwidth ".$defaultClass."; };";
  $open = fopen("./conf/".$conf."-qos.cfg", 'w+');
  fwrite($open, $output);
  fclose($open);
  }
////////////////////////////////////////////////////////////
  if ($drop_client){
    for ($k=0;$k<count($class);$k++){
      if ($class[$k][name]=="default"){
        $defaultClass=$class[$k][bandwidth];  
      }else{
        $output.= "class ".$class[$k][name]." {\n";
        $output.=  "    bandwidth ".$class[$k][bandwidth].";\n";
        $output.=   "    limit ".$class[$k][limit].";\n";
        $output.=   "    burst ".$class[$k][burst].";\n";
        $output.=   "    priority ".$class[$k][prio].";\n";
      for ($v=0;$v<count($class[$k][clients]);$v++){
        if ($client!=$class[$k][clients][$v][name]){
          $output.=   "    client ".$class[$k][clients][$v][name]." {\n";
          $output.=   "        bandwidth ".$class[$k][clients][$v][bandwidth].";\n";
          $output.=   "        limit ".$class[$k][clients][$v][limit].";\n";
          $output.=   "        burst ".$class[$k][clients][$v][burst].";\n";
          $output.=   "        priority ".$class[$k][clients][$v][prio].";\n";
          if ($class[$k][clients][$v][match][mark]){
            $output.=   "        mark ".$class[$k][clients][$v][match][mark].";\n";
          }
          if ($class[$k][clients][$v][match][src]){
            $output.=   "        src {\n";
            for ($f=0;$f<count($class[$k][clients][$v][match][src]);$f++){
              $output.=   "            ".$class[$k][clients][$v][match][src][$f].";\n";
            }
            $output.=   "        };\n";
          }
          if ($class[$k][clients][$v][match][dst]){
            $output.=   "        dst {\n";
            for ($f=0;$f<count($class[$k][clients][$v][match][dst]);$f++){
              $output.=   "            ".$class[$k][clients][$v][match][dst][$f].";\n";
            }
            $output.=   "        };\n";
          }
          $output.=   "    };\n";
        }
      }
      $output.=   "};";
      $output.=   "\n\n";  
    }
  }
  $output.="\nclass default { bandwidth ".$defaultClass."; };";
  $open = fopen("./conf/".$conf."-qos.cfg", 'w+');
  fwrite($open, $output);
  fclose($open);
  }
//////////////////////////////////////////////
  if ($add_client){
    for ($k=0;$k<count($class);$k++){
      if ($class[$k][name]=="default"){
        $defaultClass=$class[$k][bandwidth];  
      }else{
        $output.= "class ".$class[$k][name]." {\n";
        $output.=  "    bandwidth ".$class[$k][bandwidth].";\n";
        $output.=   "    limit ".$class[$k][limit].";\n";
        $output.=   "    burst ".$class[$k][burst].";\n";
        $output.=   "    priority ".$class[$k][prio].";\n";
        for ($v=0;$v<count($class[$k][clients]);$v++){
          $output.=   "    client ".$class[$k][clients][$v][name]." {\n";
          $output.=   "        bandwidth ".$class[$k][clients][$v][bandwidth].";\n";
          $output.=   "        limit ".$class[$k][clients][$v][limit].";\n";
          $output.=   "        burst ".$class[$k][clients][$v][burst].";\n";
          $output.=   "        priority ".$class[$k][clients][$v][prio].";\n";
          if ($class[$k][clients][$v][match][mark]){
            $output.=   "        mark ".$class[$k][clients][$v][match][mark].";\n";
          }
          if ($class[$k][clients][$v][match][src]){
            $output.=   "        src {\n";
            for ($f=0;$f<count($class[$k][clients][$v][match][src]);$f++){
              $output.=   "            ".$class[$k][clients][$v][match][src][$f].";\n";
            }
            $output.=   "        };\n";
          }
          if ($class[$k][clients][$v][match][dst]){
            $output.=   "        dst {\n";
            for ($f=0;$f<count($class[$k][clients][$v][match][dst]);$f++){
              $output.=   "            ".$class[$k][clients][$v][match][dst][$f].";\n";
            }
            $output.=   "        };\n";
          }
          $output.=   "    };\n";
        }
        if ($class[$k][name]==$client_class){
          $output.=   "    client ".$client_name." {\n";
          $output.=   "        bandwidth ".$client_bandwidth.";\n";
          $output.=   "        limit ".$client_limit.";\n";
          $output.=   "        burst ".$client_burst.";\n";
          $output.=   "        priority ".$client_priority.";\n";
            if ($match_mark){
              $output.=   "        mark ".$match_mark.";\n";
            }
            if ($match_src){
              $output.=   "        src {\n";
              $mMatch = split(";",$match_src);
              for ($g=0;$g<count($mMatch);$g++){
                $output.=   "            ".$mMatch[$g].";\n";
              }
              $output.=   "        };\n";
            }
            if ($match_dst){
              $output.=   "        dst {\n";
              $mMatch = split(";",$match_dst);
              for ($g=0;$g<count($mMatch);$g++){
                $output.=   "            ".$mMatch[$g].";\n";
              }
              $output.=   "        };\n";
              }
          $output.=   "    };\n";
        }      
        $output.=   "};";
        $output.=   "\n\n";  
      }
    }
    $output.="\nclass default { bandwidth ".$defaultClass."; };";
  
    $open = fopen("./conf/".$conf."-qos.cfg", 'w+');
    fwrite($open, $output);
    fclose($open);
  }
//////////////////////////////////////////////////////
  if ($edit_class){
    for ($k=0;$k<count($class);$k++){
      if ($class[$k][name]=="default"){
        $defaultClass=$class[$k][bandwidth];  
      }else{
      if ($class[$k][name]==$class_name){
      $output.="class ".$class_name." {\n";
      $output.="    bandwidth ".$class_bandwidth.";\n";
      $output.="    limit ".$class_limit.";\n";
      $output.="    burst ".$class_burst.";\n";
      $output.="    priority ".$class_priority.";\n";
      $output.="};\n";
      for ($v=0;$v<count($class[$k][clients]);$v++){
        $output.=   "    client ".$class[$k][clients][$v][name]." {;\n";
        $output.=   "        bandwidth ".$class[$k][clients][$v][bandwidth].";\n";
        $output.=   "        limit ".$class[$k][clients][$v][limit].";\n";
        $output.=   "        burst ".$class[$k][clients][$v][burst].";\n";
        $output.=   "        priority ".$class[$k][clients][$v][prio].";\n";
        if ($class[$k][clients][$v][match][mark]){
          $output.=   "        mark ".$class[$k][clients][$v][match][mark].";\n";
        }
        if ($class[$k][clients][$v][match][src]){
          $output.=   "        src {\n";
          for ($f=0;$f<count($class[$k][clients][$v][match][src]);$f++){
            $output.=   "            ".$class[$k][clients][$v][match][src][$f].";\n";
          }
          $output.=   "        };\n";
        }
        if ($class[$k][clients][$v][match][dst]){
          $output.=   "        dst {\n";
          for ($f=0;$f<count($class[$k][clients][$v][match][dst]);$f++){
            $output.=   "            ".$class[$k][clients][$v][match][dst][$f].";\n";
          }
          $output.=   "        };\n";
        }
        $output.=   "    };\n";
      }
    }else{
      $output.= "class ".$class[$k][name]." {\n";
      $output.=  "    bandwidth ".$class[$k][bandwidth].";\n";
      $output.=   "    limit ".$class[$k][limit].";\n";
      $output.=   "    burst ".$class[$k][burst].";\n";
      $output.=   "    priority ".$class[$k][prio].";\n";
      for ($v=0;$v<count($class[$k][clients]);$v++){
        $output.=   "    client ".$class[$k][clients][$v][name]." {\n";
        $output.=   "        bandwidth ".$class[$k][clients][$v][bandwidth].";\n";
        $output.=   "        limit ".$class[$k][clients][$v][limit].";\n";
        $output.=   "        burst ".$class[$k][clients][$v][burst].";\n";
        $output.=   "        priority ".$class[$k][clients][$v][prio].";\n";
        if ($class[$k][clients][$v][match][mark]){
          $output.=   "        mark ".$class[$k][clients][$v][match][mark].";\n";
        }
        if ($class[$k][clients][$v][match][src]){
          $output.=   "        src {\n";
          for ($f=0;$f<count($class[$k][clients][$v][match][src]);$f++){
            $output.=   "            ".$class[$k][clients][$v][match][src][$f].";\n";
          }
          $output.=   "        };\n";
        }
        if ($class[$k][clients][$v][match][dst]){
          $output.=   "        dst {\n";
          for ($f=0;$f<count($class[$k][clients][$v][match][dst]);$f++){
            $output.=   "            ".$class[$k][clients][$v][match][dst][$f].";\n";
          }
          $output.=   "        };\n";
        }
        $output.=   "    };\n";
      }
      $output.=   "};";
      $output.=   "\n\n";  
    }
  }
  }
  $output.="\nclass default { bandwidth ".$defaultClass."; };";
  $open = fopen("./conf/".$conf."-qos.cfg", 'w+');
  fwrite($open, $output);
  fclose($open);
  }
/////////////////////////////////////////////////////
  if($add_class){
    for ($k=0;$k<count($class);$k++){
      if ($class[$k][name]=="default"){
        $defaultClass=$class[$k][bandwidth];  
      }else{
        $output.= "class ".$class[$k][name]." {\n";
        $output.=  "    bandwidth ".$class[$k][bandwidth]."\n";
        $output.=   "    limit ".$class[$k][limit]."\n";
        $output.=   "    burst ".$class[$k][burst]."\n";
        $output.=   "    priority ".$class[$k][prio]."\n";
      for ($v=0;$v<count($class[$k][clients]);$v++){
        $output.=   "    client ".$class[$k][clients][$v][name]." {\n";
        $output.=   "        bandwidth ".$class[$k][clients][$v][bandwidth]."\n";
        $output.=   "        limit ".$class[$k][clients][$v][limit]."\n";
        $output.=   "        burst ".$class[$k][clients][$v][burst]."\n";
        $output.=   "        priority ".$class[$k][clients][$v][prio]."\n";
        if ($class[$k][clients][$v][match][mark]){
          $output.=   "        mark ".$class[$k][clients][$v][match][mark]."\n";
        }
        if ($class[$k][clients][$v][match][src]){
          $output.=   "        src {\n";
          for ($f=0;$f<count($class[$k][clients][$v][match][src]);$f++){
            $output.=   "            ".$class[$k][clients][$v][match][src][$f]."\n";
          }
          $output.=   "        };\n";
        }
        if ($class[$k][clients][$v][match][dst]){
          $output.=   "        dst {\n";
          for ($f=0;$f<count($class[$k][clients][$v][match][dst]);$f++){
            $output.=   "            ".$class[$k][clients][$v][match][dst][$f]."\n";
          }
          $output.=   "        };\n";
        }
        $output.=   "    };\n";
      }
      $output.=   "};";
      $output.=   "\n\n";  
    }
  }
  $output.="class ".$class_name." {\n";
  $output.="    bandwidth ".$class_bandwidth.";\n";
  $output.="    limit ".$class_limit.";\n";
  $output.="    burst ".$class_burst.";\n";
  $output.="    priority ".$class_priority.";\n";
  $output.="};\n";
  $output.="\nclass default { bandwidth ".$defaultClass."; };";
  $open = fopen("./conf/".$conf."-qos.cfg", 'w+');
  fwrite($open, $output);
  fclose($open);
  } 
///////////////////////////////////////////
  if($class_drop){
    for ($k=0;$k<count($class);$k++){
      if ($class[$k][name]=="default"){
        $defaultClass=$class[$k][bandwidth];  
      }else{
        if ($class_!=$class[$k][name]){
        $output.= "class ".$class[$k][name]." {\n";
        $output.=  "    bandwidth ".$class[$k][bandwidth]."\n";
        $output.=   "    limit ".$class[$k][limit]."\n";
        $output.=   "    burst ".$class[$k][burst]."\n";
        $output.=   "    priority ".$class[$k][prio]."\n";
        for ($v=0;$v<count($class[$k][clients]);$v++){
          $output.=   "    client ".$class[$k][clients][$v][name]." {\n";
          $output.=   "        bandwidth ".$class[$k][clients][$v][bandwidth]."\n";
          $output.=   "        limit ".$class[$k][clients][$v][limit]."\n";
          $output.=   "        burst ".$class[$k][clients][$v][burst]."\n";
          $output.=   "        priority ".$class[$k][clients][$v][prio]."\n";
          if ($class[$k][clients][$v][match][mark]){
            $output.=   "        mark ".$class[$k][clients][$v][match][mark]."\n";
          }
          if ($class[$k][clients][$v][match][src]){
            $output.=   "        src {\n";
            for ($f=0;$f<count($class[$k][clients][$v][match][src]);$f++){
              $output.=   "            ".$class[$k][clients][$v][match][src][$f]."\n";
            }
            $output.=   "        };\n";
          }
          if ($class[$k][clients][$v][match][dst]){
            $output.=   "        dst {\n";
            for ($f=0;$f<count($class[$k][clients][$v][match][dst]);$f++){
              $output.=   "            ".$class[$k][clients][$v][match][dst][$f]."\n";
            }
            $output.=   "        };\n";
          }
          $output.=   "    };\n";
        }
        $output.=   "};";
        $output.=   "\n\n";  
      }
    }
  }
  $output.="\nclass default { bandwidth ".$defaultClass."; };";
  $open = fopen("./conf/".$conf."-qos.cfg", 'w+');
  fwrite($open, $output);
  fclose($open);
  }
//////////////////////////////////////////////////////
?>
<font face=arial size=2 color=black><div align=right>
<center>
<b>
<?
  echo "Curent config file: ".$conf."-qos.cfg";
?>
</b>
</center>
<br>
<a href="d_add_class.php?conf=<?php echo $conf; ?>" style="text-decoration: none"> Add class </a>|
<a href="d_add_client.php?conf=<?php echo $conf; ?>" style="text-decoration: none"> Add client </a>|
<a href="show_conf.php?file=<?php echo $conf; ?>" target="_blabla" style="text-decoration: none"> Show config file <a>|
<a href="conf_details.php?drop=<?php echo $conf; ?>" style="text-decoration: none"> Drop config</a>
<hr>
<br>
<center>
<?php
  $class = read_conf($conf); // refresh
  echo "<table border=0>"; 
  for ($k=0;$k<count($class);$k++){
    if ($class[$k][name]=="default"){
      $defaultBandwidth= $class[$k][bandwidth];
    }else{
      echo  "<tr><td colspan=3><hr></td></tr>";
      echo "<tr><td><font face=arial size=2><b>class ".$class[$k][name]."</b></td>";
      echo "<td colspan=2><font face=arial size=2>bandwidth ".$class[$k][bandwidth]."</td></tr>";
      echo "<tr><td><font face=arial size=2><a href=\"d_edit_class.php?conf=".$conf."&class_=".$class[$k][name]."\" style=\"text-decoration: none\">edit</a> | ";
      echo "<a href=\"conf_details.php?conf=".$conf."&class_drop=true&class_=".$class[$k][name]."\" style=\"text-decoration: none\">drop</a></td>";
      echo "<td colspan=2><font face=arial size=2>limit ".$class[$k][limit]."</td></tr>";
      echo "<tr><td></td><td colspan=2><font face=arial size=2>burst ".$class[$k][burst]."</td></tr>";
      echo "<tr><td></td><td colspan=2><font face=arial size=2>priority ".$class[$k][prio]."</td></tr>";
      echo "<tr><td></td><td colspan=2><hr></td></tr>";
      for ($v=0;$v<count($class[$k][clients]);$v++){
        echo "<tr><td></td><td><font face=arial size=2><b>client ".$class[$k][clients][$v][name]."</b></td>";
        echo "<td><font face=arial size=2>bandwidth ".$class[$k][clients][$v][bandwidth]."</td></tr>";
        echo "<tr><td></td><td><font face=arial size=2><a href=\"d_edit_client.php?conf=".$conf."&client=".$class[$k][clients][$v][name]."\" style=\"text-decoration: none\">edit</a> | ";
        echo "<a href=\"conf_details.php?drop_client=true&conf=".$conf."&client=".$class[$k][clients][$v][name]."\" style=\"text-decoration: none\">drop</a></td>";
        echo "<td><font face=arial size=2>limit ".$class[$k][clients][$v][limit]."</td></tr>";
        echo "<tr><td></td><td></td><td><font face=arial size=2>burst ".$class[$k][clients][$v][burst]."</td></tr>";
        echo "<tr><td></td><td></td><td><font face=arial size=2>priority ".$class[$k][clients][$v][prio]."</td></tr>";
        if ($class[$k][clients][$v][match][mark]){
          echo "<tr><td></td><td></td><td><font face=arial size=2><b>mark: </b>".$class[$k][clients][$v][match][mark]."</td></tr>";
        }
        if ($class[$k][clients][$v][match][src]){
          echo "<tr><td></td><td></td><td><font face=arial size=2><b>src: </b>";
          for ($f=0;$f<count($class[$k][clients][$v][match][src]);$f++){
            echo $class[$k][clients][$v][match][src][$f]."<br>";
          }
          echo "</td></tr>";
        }
        if ($class[$k][clients][$v][match][dst]){
          echo "<tr><td></td><td></td><td><font face=arial size=2><b>dst: </b>";
          for ($f=0;$f<count($class[$k][clients][$v][match][dst]);$f++){
            echo $class[$k][clients][$v][match][dst][$f]."<br>";
          }
        echo "</td></tr>";  
        }
        echo "<tr><td></td><td colspan=2><hr></td></tr>";
      }
    }
  }
  echo "<tr><td><font face=arial size=2><b>class default</b></td>";
  echo "<td colspan=2><font face=arial size=2>bandwidth ".$defaultBandwidth."</td></tr>";
  echo  "<tr><td colspan=3><hr></td></tr>";
  echo "</table>";
?> 
<br><br>
<br><br><hr><center>
<b>WEB-HTB_Tools</b>
<br>
<i>By Daniel Adrian Gard
